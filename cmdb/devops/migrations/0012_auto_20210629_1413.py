# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2021-06-29 14:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devops', '0011_sysconfigfilemap'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sysconfigfilemap',
            options={},
        ),
        migrations.AddField(
            model_name='sysconfigfilemap',
            name='type',
            field=models.CharField(blank=True, max_length=80, null=True),
        ),
    ]
