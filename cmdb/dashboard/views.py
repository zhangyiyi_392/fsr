import psutil
import json
from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User, Group
from resources.models import NewServer
from devops.models import AutoReCovery,MonitorConfig
from alert.models import AlertHistory
from users.models import Profile
from django.shortcuts import render_to_response,HttpResponse
# Create your views here.

class IndexView(LoginRequiredMixin, TemplateView):
	template_name = 'index.html'

	def get_context_data(self, **kwargs):
		context = super(IndexView, self).get_context_data(**kwargs)
		context['user_count'] = User.objects.all().count()
		context['group_count'] = Group.objects.all().count()
		context['host_count'] = NewServer.objects.all().count()
		context['autorecovery_count'] = AutoReCovery.objects.all().count()
		context['monitorconfig_count'] = MonitorConfig.objects.all().count()
		context['alerthistory_count'] = AlertHistory.objects.all().count()
		context['host_error_count'] = NewServer.objects.filter(scan_status=0).count()
		context['server_list'] = NewServer.objects.all().order_by('-update_date')
		return context

