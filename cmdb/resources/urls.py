# _*_ coding: utf-8 _*_
__author__ = 'HaoGe'
from django.conf.urls import url, include

from resources.servers import views as server_views
from resources.groups import views as group_views
from resources.serveruser import views as server_user_views

urlpatterns = [
    url(r'servers/', include([
        url(r'list/$', server_views.ServerListView.as_view(), name='server_list'),
        url(r'create/$', server_views.ServerCreateView.as_view(), name='server_create'),
        url(r'edit/$', server_views.ServerEditView.as_view(), name='newserver_edit'),
        url(r'data_api/$', server_views.ServerDataApiView.as_view(), name='server_data_api'),
        url(r'detail/(?P<pk>\d+)/$', server_views.ServerDetailView.as_view(), name='server_detail'),
        url(r'modify_idc/$', server_views.ServerModifyIdcView.as_view(), name='server_modify_idc'),
        url(r'delete/$', server_views.ServerDeleteView.as_view(), name='server_delete'),
        url(r'flush/$', server_views.ServerFlushView.as_view(), name='server_flush'),
        url(r'get/$', server_views.ServerGetListView.as_view(), name='server_get'),
        url(r'set_product/$', server_views.ServerSetProduct.as_view(), name='server_set_product'),
        url(r'load/$', server_views.ServerLoadView.as_view(), name='server_load'),
        url(r'get_host_connect/$', server_views.HostConnectView.as_view(), name='get_host_connect'),
    ])),
    url(r'group/', include([
        url(r'list/$', group_views.GroupListView.as_view(), name='servergroup_list'),
        url(r'create/$', group_views.GroupCreateView.as_view(), name='servergroup_create'),
        url(r'edit/$', group_views.GroupEditView.as_view(), name='servergroup_edit'),
        url(r'delete/$', group_views.GroupDeleteView.as_view(), name='servergroup_delete'),
    ])),
    url(r'serveruser/', include([
        url(r'list/$', server_user_views.ServerUserListView.as_view(), name='serveruser_list'),
        url(r'create/$', server_user_views.ServerUserCreateView.as_view(), name='serveruser_create'),
        url(r'modify/$', server_user_views.ServerUserModifyView.as_view(), name='serveruser_modify'),
        url(r'delete/$', server_user_views.ServerUserDeleteView.as_view(), name='serveruser_delete'),
        url(r'detail/$', server_user_views.ServerUserDetailView.as_view(), name='serveruser_detail'),
    ]))
]