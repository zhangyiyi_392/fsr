# _*_ coding: utf-8 _*_
__author__ = 'HaoGe'
from django import template

# register固定写法
register = template.Library()

@register.filter
def serveruser_count(server_list):
    return len(list(server_list))